const express = require("express");
const app = express();
const cors = require("cors");
const bodyParser = require("body-parser");
const kurikulumRoute = require("./routes/kurikulumRoute");
const orangtuaRoute = require("./routes/orangtuaRoute");
const pesertadidikRoute = require("./routes/pesertadidikRoute");
const ptkRoute = require("./routes/ptkRoute");
const sekolahRoute = require("./routes/sekolahRoute");
const refRoute = require("./routes/refRoute");
const kbmRoute = require("./routes/kbmRoute");
// var corsOptions = {
//   origin: '*',
//   methods: ['GET', 'PUT', 'POST', 'HEAD', 'DELETE', 'OPTIONS'],
//   allowedHeaders: ['Content-Type', 'Authorization', 'Origin', 'Access-Control-Allow-Origin'],
//   credentials: true,
// }
app.use(cors());

var urlencodedParser = bodyParser.urlencoded({ extended: false });
app.use(express.json());
app.use(urlencodedParser);
app.use("/kurikulum", kurikulumRoute);
app.use("/orang_tua", orangtuaRoute);
app.use("/peserta_didik", pesertadidikRoute);
app.use("/ptk", ptkRoute);
app.use("/sekolah", sekolahRoute);
app.use("/ref", refRoute);
app.use('/akademik', kbmRoute);

const PORT = 5000 ;

app.listen(PORT, () => {
  console.log(`listening on port ${PORT}`);
});
