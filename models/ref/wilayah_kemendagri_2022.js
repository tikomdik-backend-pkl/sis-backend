const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('wilayah_kemendagri_2022', {
    id_wilayah: {
      type: DataTypes.STRING(10),
      allowNull: true
    },
    nama: {
      type: DataTypes.STRING(120),
      allowNull: true
    }
  }, {
    tableName: 'wilayah_kemendagri_2022',
    schema: 'ref',
    timestamps: false
  });
};
