const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('sumber_gaji', {
    sumber_gaji_id: {
      type: DataTypes.DECIMAL,
      allowNull: false,
      primaryKey: true
    },
    nama: {
      type: DataTypes.STRING(40),
      allowNull: false
    },
    create_date: {
      type: DataTypes.DATE,
      allowNull: false
    },
    last_update: {
      type: DataTypes.DATE,
      allowNull: false
    },
    expired_date: {
      type: DataTypes.DATE,
      allowNull: true
    },
    last_sync: {
      type: DataTypes.DATE,
      allowNull: false
    }
  }, {
    tableName: 'sumber_gaji',
    schema: 'ref',
    timestamps: false
  });
};
