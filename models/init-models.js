var DataTypes = require("sequelize").DataTypes;
var _golongan_darah = require("./golongan_darah");
var _jadwal_tk10_11 = require("./jadwal_tk10_11");
var _kurikulum = require("./kurikulum");
var _kurikulum_anggota_rombel = require("./kurikulum_anggota_rombel");
var _kurikulum_program = require("./kurikulum_program");
var _kurikulum_rombongan_belajar = require("./kurikulum_rombongan_belajar");
var _orangtua_alamat = require("./orangtua_alamat");
var _penghasilan = require("./penghasilan");
var _peserta_didik = require("./peserta_didik");
var _peserta_didik_alamat = require("./peserta_didik_alamat");
var _peserta_didik_kesehatan = require("./peserta_didik_kesehatan");
var _peserta_didik_kontak = require("./peserta_didik_kontak");
var _peserta_didik_rekening = require("./peserta_didik_rekening");
var _ptk = require("./ptk");
var _ptk_pangkat_gol = require("./ptk_pangkat_gol");
var _ptk_pend_formal = require("./ptk_pend_formal");
var _ptk_temp = require("./ptk_temp");
var _ptk_tugas_mengajar = require("./ptk_tugas_mengajar");
var _sekolah_akreditasi = require("./sekolah_akreditasi");
var _sekolah_alamat = require("./sekolah_alamat");
var _sekolah_bank = require("./sekolah_bank");
var _sekolah_identitas = require("./sekolah_identitas");
var _sekolah_kepemilikan = require("./sekolah_kepemilikan");
var _sekolah_sertifikasi_iso = require("./sekolah_sertifikasi_iso");
var _jadwal_kbm = require("./kbm/jadwal_kbm");
var _mapel_sp = require("./kbm/mapel_sp");
var _merdeka_capaian_sp = require("./kbm/merdeka_capaian_sp");
var _merdeka_elemen_sp = require("./kbm/merdeka_elemen_sp");
var _ptk_mapel = require("./kbm/ptk_mapel");
var _agama = require("./ref/agama");
var _akreditasi = require("./ref/akreditasi");
var _alasan_layak_pip = require("./ref/alasan_layak_pip");
var _alat_transportasi = require("./ref/alat_transportasi");
var _bank = require("./ref/bank");
var _bentuk_pendidikan = require("./ref/bentuk_pendidikan");
var _gelar_akademik = require("./ref/gelar_akademik");
var _hari = require("./ref/hari");
var _jabatan_tugas_ptk = require("./ref/jabatan_tugas_ptk");
var _jenis_kewarganegaraan = require("./ref/jenis_kewarganegaraan");
var _jenis_pendaftaran = require("./ref/jenis_pendaftaran");
var _jenis_ptk = require("./ref/jenis_ptk");
var _jenis_rombel = require("./ref/jenis_rombel");
var _jenis_tinggal = require("./ref/jenis_tinggal");
var _jenjang_pendidikan = require("./ref/jenjang_pendidikan");
var _jurusan = require("./ref/jurusan");
var _keahlian_laboratorium = require("./ref/keahlian_laboratorium");
var _lembaga_pengangkat = require("./ref/lembaga_pengangkat");
var _merdeka_capaian = require("./ref/merdeka_capaian");
var _merdeka_elemen = require("./ref/merdeka_elemen");
var _merdeka_mapel = require("./ref/merdeka_mapel");
var _pangkat_golongan = require("./ref/pangkat_golongan");
var _pekerjaan = require("./ref/pekerjaan");
var _program_studi = require("./ref/program_studi");
var _ref_sekolah = require("./ref/ref_sekolah");
var _semester = require("./ref/semester");
var _sertifikasi_iso = require("./ref/sertifikasi_iso");
var _status_kepegawaian = require("./ref/status_kepegawaian");
var _status_kepemilikan = require("./ref/status_kepemilikan");
var _status_perkawinan = require("./ref/status_perkawinan");
var _sumber_gaji = require("./ref/sumber_gaji");
var _tahun_ajaran = require("./ref/tahun_ajaran");
var _tingkat_pendidikan = require("./ref/tingkat_pendidikan");
var _waktu_penyelenggaraan = require("./ref/waktu_penyelenggaraan");
var _wilayah_kemendagri_2022 = require("./ref/wilayah_kemendagri_2022");

function initModels(sequelize) {
  var golongan_darah = _golongan_darah(sequelize, DataTypes);
  var jadwal_tk10_11 = _jadwal_tk10_11(sequelize, DataTypes);
  var kurikulum = _kurikulum(sequelize, DataTypes);
  var kurikulum_anggota_rombel = _kurikulum_anggota_rombel(sequelize, DataTypes);
  var kurikulum_program = _kurikulum_program(sequelize, DataTypes);
  var kurikulum_rombongan_belajar = _kurikulum_rombongan_belajar(sequelize, DataTypes);
  var orangtua_alamat = _orangtua_alamat(sequelize, DataTypes);
  var penghasilan = _penghasilan(sequelize, DataTypes);
  var peserta_didik = _peserta_didik(sequelize, DataTypes);
  var peserta_didik_alamat = _peserta_didik_alamat(sequelize, DataTypes);
  var peserta_didik_kesehatan = _peserta_didik_kesehatan(sequelize, DataTypes);
  var peserta_didik_kontak = _peserta_didik_kontak(sequelize, DataTypes);
  var peserta_didik_rekening = _peserta_didik_rekening(sequelize, DataTypes);
  var ptk = _ptk(sequelize, DataTypes);
  var ptk_pangkat_gol = _ptk_pangkat_gol(sequelize, DataTypes);
  var ptk_pend_formal = _ptk_pend_formal(sequelize, DataTypes);
  var ptk_temp = _ptk_temp(sequelize, DataTypes);
  var ptk_tugas_mengajar = _ptk_tugas_mengajar(sequelize, DataTypes);
  var sekolah_akreditasi = _sekolah_akreditasi(sequelize, DataTypes);
  var sekolah_alamat = _sekolah_alamat(sequelize, DataTypes);
  var sekolah_bank = _sekolah_bank(sequelize, DataTypes);
  var sekolah_identitas = _sekolah_identitas(sequelize, DataTypes);
  var sekolah_kepemilikan = _sekolah_kepemilikan(sequelize, DataTypes);
  var sekolah_sertifikasi_iso = _sekolah_sertifikasi_iso(sequelize, DataTypes);
  var jadwal_kbm = _jadwal_kbm(sequelize, DataTypes);
  var mapel_sp = _mapel_sp(sequelize, DataTypes);
  var merdeka_capaian_sp = _merdeka_capaian_sp(sequelize, DataTypes);
  var merdeka_elemen_sp = _merdeka_elemen_sp(sequelize, DataTypes);
  var ptk_mapel = _ptk_mapel(sequelize, DataTypes);
  var agama = _agama(sequelize, DataTypes);
  var akreditasi = _akreditasi(sequelize, DataTypes);
  var alasan_layak_pip = _alasan_layak_pip(sequelize, DataTypes);
  var alat_transportasi = _alat_transportasi(sequelize, DataTypes);
  var bank = _bank(sequelize, DataTypes);
  var bentuk_pendidikan = _bentuk_pendidikan(sequelize, DataTypes);
  var gelar_akademik = _gelar_akademik(sequelize, DataTypes);
  var hari = _hari(sequelize, DataTypes);
  var jabatan_tugas_ptk = _jabatan_tugas_ptk(sequelize, DataTypes);
  var jenis_kewarganegaraan = _jenis_kewarganegaraan(sequelize, DataTypes);
  var jenis_pendaftaran = _jenis_pendaftaran(sequelize, DataTypes);
  var jenis_ptk = _jenis_ptk(sequelize, DataTypes);
  var jenis_rombel = _jenis_rombel(sequelize, DataTypes);
  var jenis_tinggal = _jenis_tinggal(sequelize, DataTypes);
  var jenjang_pendidikan = _jenjang_pendidikan(sequelize, DataTypes);
  var jurusan = _jurusan(sequelize, DataTypes);
  var keahlian_laboratorium = _keahlian_laboratorium(sequelize, DataTypes);
  var lembaga_pengangkat = _lembaga_pengangkat(sequelize, DataTypes);
  var merdeka_capaian = _merdeka_capaian(sequelize, DataTypes);
  var merdeka_elemen = _merdeka_elemen(sequelize, DataTypes);
  var merdeka_mapel = _merdeka_mapel(sequelize, DataTypes);
  var pangkat_golongan = _pangkat_golongan(sequelize, DataTypes);
  var pekerjaan = _pekerjaan(sequelize, DataTypes);
  var program_studi = _program_studi(sequelize, DataTypes);
  var ref_sekolah = _ref_sekolah(sequelize, DataTypes);
  var semester = _semester(sequelize, DataTypes);
  var sertifikasi_iso = _sertifikasi_iso(sequelize, DataTypes);
  var status_kepegawaian = _status_kepegawaian(sequelize, DataTypes);
  var status_kepemilikan = _status_kepemilikan(sequelize, DataTypes);
  var status_perkawinan = _status_perkawinan(sequelize, DataTypes);
  var sumber_gaji = _sumber_gaji(sequelize, DataTypes);
  var tahun_ajaran = _tahun_ajaran(sequelize, DataTypes);
  var tingkat_pendidikan = _tingkat_pendidikan(sequelize, DataTypes);
  var waktu_penyelenggaraan = _waktu_penyelenggaraan(sequelize, DataTypes);
  var wilayah_kemendagri_2022 = _wilayah_kemendagri_2022(sequelize, DataTypes);

  return {
    golongan_darah,
    jadwal_tk10_11,
    kurikulum,
    kurikulum_anggota_rombel,
    kurikulum_program,
    kurikulum_rombongan_belajar,
    orangtua_alamat,
    penghasilan,
    peserta_didik,
    peserta_didik_alamat,
    peserta_didik_kesehatan,
    peserta_didik_kontak,
    peserta_didik_rekening,
    ptk,
    ptk_pangkat_gol,
    ptk_pend_formal,
    ptk_temp,
    ptk_tugas_mengajar,
    sekolah_akreditasi,
    sekolah_alamat,
    sekolah_bank,
    sekolah_identitas,
    sekolah_kepemilikan,
    sekolah_sertifikasi_iso,
    jadwal_kbm,
    mapel_sp,
    merdeka_capaian_sp,
    merdeka_elemen_sp,
    ptk_mapel,
    agama,
    akreditasi,
    alasan_layak_pip,
    alat_transportasi,
    bank,
    bentuk_pendidikan,
    gelar_akademik,
    hari,
    jabatan_tugas_ptk,
    jenis_kewarganegaraan,
    jenis_pendaftaran,
    jenis_ptk,
    jenis_rombel,
    jenis_tinggal,
    jenjang_pendidikan,
    jurusan,
    keahlian_laboratorium,
    lembaga_pengangkat,
    merdeka_capaian,
    merdeka_elemen,
    merdeka_mapel,
    pangkat_golongan,
    pekerjaan,
    program_studi,
    ref_sekolah,
    semester,
    sertifikasi_iso,
    status_kepegawaian,
    status_kepemilikan,
    status_perkawinan,
    sumber_gaji,
    tahun_ajaran,
    tingkat_pendidikan,
    waktu_penyelenggaraan,
    wilayah_kemendagri_2022,
  };
}
module.exports = initModels;
module.exports.initModels = initModels;
module.exports.default = initModels;
