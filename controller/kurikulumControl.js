const {findAll, Create, Update, Delete} = require('.')

exports.gKurikulum = async (req, res) => {
    const page = req.query.page || 0;
    const limit = req.query.limit || 5;
    const properties = { limit: limit, offset: limit * page }; 

    const data = await findAll('kurikulum', properties)
    res.send(data)
}
exports.pKurikulum = async (req, res) => {
    const page = req.query.page || 0;
    const limit = req.query.limit || 5;
    const properties = { limit: limit, offset: limit * page };
    const payload = req.body 

    const data = await Create('kurikulum', properties, payload)
    if (data.error) {
        return res.status(data.status).send(data.error);
      }
    res.send(data)
}

exports.gAnggota_rombel = async (req, res) => {
    const page = req.query.page || 0;
    const limit = req.query.limit || 5;
    const properties = { limit: limit, offset: limit * page }; 

    const data = await findAll('kurikulum_anggota_rombel', properties)
    res.send(data)
}
exports.pAnggota_rombel = async (req, res) => {
    const page = req.query.page || 0;
    const limit = req.query.limit || 5;
    const properties = { limit: limit, offset: limit * page }; 
    const payload = req.body 

    const data = await Create('kurikulum_anggota_rombel', properties, payload)
    if (data.error) {
        return res.status(data.status).send(data.error);
      }
    res.send(data)
}
exports.gRombel = async (req, res) => {
    const page = req.query.page || 0;
    const limit = req.query.limit || 5;
    const properties = { limit: limit, offset: limit * page }; 

    const data = await findAll('kurikulum_rombongan_belajar', properties)
    res.send(data)
}
exports.pRombel = async (req, res) => {
    const page = req.query.page || 0;
    const limit = req.query.limit || 5;
    const properties = { limit: limit, offset: limit * page }; 
    const payload = req.body

    const data = await Create('kurikulum_rombongan_belajar', properties, payload)
    if (data.error) {
        return res.status(data.status).send(data.error);
      }
    res.send(data)
}
exports.gProgram = async (req, res) => {
    const page = req.query.page || 0;
    const limit = req.query.limit || 5;
    const properties = { limit: limit, offset: limit * page }; 

    const data = await findAll('kurikulum_program', properties)
    res.send(data)
}
exports.pProgram = async (req, res) => {
    const page = req.query.page || 0;
    const limit = req.query.limit || 5;
    const properties = { limit: limit, offset: limit * page };
    const payload = req.body 

    const data = await Create('kurikulum_program', properties, payload)
    if (data.error) {
        return res.status(data.status).send(data.error);
      }
    res.send(data)
}
exports.dkurikulum = async (req, res) => {
    const id = req.params.id
    const properties = { where : { 'kurikulum_sp_id' : id }}; 

    const data = await Delete('kurikulum', properties)
    res.send(data)
}
exports.dkurikulum_anggota_rombel = async (req, res) => {
    const id = req.params.id
    const properties = { where : { 'anggota_rombel_id' : id }}; 

    const data = await Delete('kurikulum_anggota_rombel', properties)
    res.send(data)
}
exports.dkurikulum_program = async (req, res) => {
    const id = req.params.id
    const properties = { where : { 'kurikulum_program_id' : id }}; 

    const data = await Delete('kurikulum_program', properties)
    res.send(data)
}
exports.dkurikulum_rombongan_belajar = async (req, res) => {
    const id = req.params.id
    const properties = { where : { 'rombongan_belajar_id' : id }}; 
    
    const data = await Delete('kurikulum_rombongan_belajar', properties)
}
exports.ukurikulum = async (req, res) => {
    const payload = req.body
    const id = req.params.id
    const properties = { where : { 'kurikulum_sp_id' : id }}; 
    const data = await Update('kurikulum', payload, properties)
    res.send(data)
}
exports.ukurikulum_anggota_rombel = async (req, res) => {
    const payload = req.body
    const id = req.params.id
    const properties = { where : { 'anggota_rombel_id' : id }}; 
    const data = await Update('kurikulum_anggota_rombel', payload, properties)
    res.send(data)
}
exports.ukurikulum_program = async (req, res) => {
    const payload = req.body
    const id = req.params.id
    const properties = { where : { 'kurikulum_program_id' : id }}; 
    const data = await Update('kurikulum_program', payload, properties)
    res.send(data)
}
exports.ukurikulum_rombongan_belajar = async (req, res) => {
    const payload = req.body
    const id = req.params.id
    const properties = { where : { 'rombongan_belajar_id' : id }};
    const data = await Update('kurikulum_rombongan_belajar', payload, properties)
    res.send(data)
}

