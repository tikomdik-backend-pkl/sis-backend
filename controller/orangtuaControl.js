const {findAll, Create, Update, Delete} = require('.')

exports.gAlamat = async (req, res) => {
    const page = req.query.page || 0;
    const limit = req.query.limit || 5;
    const properties = { limit: limit, offset: limit * page }; 

    const data = await findAll('orangtua_alamat', properties)
    res.send(data)
}
exports.pAlamat = async (req, res) => {
    const page = req.query.page || 0;
    const limit = req.query.limit || 5;
    const properties = { limit: limit, offset: limit * page }; 
    const payload = req.body
    const data = await Create('orangtua_alamat', properties, payload)
    if (data.error) {
        return res.status(data.status).send(data.error);
      }
    res.send(data)
}
exports.dAlamat = async (req, res) => {
    const id = req.params.id
    const properties = { where : { 'orangtua_alamat_id': id }}; 

    const data = await Delete('orangtua_alamat', properties)
    res.send(data)
}
exports.uAlamat = async (req, res) => {
    const payload = req.body
    const id = req.params.id
    const properties = { where : { 'orangtua_alamat_id' : id }}; 

    const data = await Update('orangtua_alamat', payload, properties)
    res.send(data)
}
exports.gpenghasilan = async (req, res) => {
    const page = req.query.page || 0;
    const limit = req.query.limit || 5;
    const properties = { limit: limit, offset: limit * page }; 

    const data = await findAll('penghasilan', properties)
    res.send(data)
}
exports.ppenghasilan = async (req, res) => {
    const page = req.query.page || 0;
    const limit = req.query.limit || 5;
    const properties = { limit: limit, offset: limit * page }; 
    const payload = req.body
    const data = await Create('penghasilan', properties, payload)
    if (data.error) {
        return res.status(data.status).send(data.error);
      }
    res.send(data)
}
exports.dpenghasilan = async (req, res) => {
    const id = req.params.id
    const properties = { where : { 'penghasilan_id': id }}; 

    const data = await Delete('penghasilan', properties)
    res.send(data)
}
exports.upenghasilan = async (req, res) => {
    const payload = req.body
    const id = req.params.id
    const properties = { where : { 'penghasilan_id' : id }}; 

    const data = await Update('penghasilan', payload, properties)
    res.send(data)
}

