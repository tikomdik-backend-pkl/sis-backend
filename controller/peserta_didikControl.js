const {findAll, Create, Update, Delete} = require('.')

exports.gAlamat = async (req, res) => {
    const page = req.query.page || 0;
    const limit = req.query.limit || 5;
    const properties = { limit: limit, offset: limit * page }; 

    const data = await findAll('peserta_didik_alamat', properties)
    res.send(data)
}
exports.gKontak = async (req, res) => {
    const page = req.query.page || 0;
    const limit = req.query.limit || 5;
    const properties = { limit: limit, offset: limit * page }; 

    const data = await findAll('peserta_didik_kontak', properties)
    res.send(data)
}
exports.gPeserta_didik = async (req, res) => {
    const page = req.query.page || 0;
    const limit = req.query.limit || 15;
    const properties = { limit: limit, offset: limit * page }; 

    const data = await findAll('peserta_didik', properties)
    res.send(data)
}
exports.pPeserta_didik = async (req, res) => {
    const page = req.query.page || 0;
    const limit = req.query.limit || 5;
    const properties = { limit: limit, offset: limit * page }; 
    const payload = req.body
    const data = await Create('peserta_didik', properties, payload)
    if (data.error) {
      return res.status(data.status).send(data.error);
    }
    res.send(data)
}
exports.gPeserta_didik_kesehatan = async (req, res) => {
    const page = req.query.page || 0;
    const limit = req.query.limit || 15;
    const properties = { limit: limit, offset: limit * page }; 

    const data = await findAll('peserta_didik_kesehatan', properties)
    res.send(data)
}
exports.pPeserta_didik_kesehatan = async (req, res) => {
    const page = req.query.page || 0;
    const limit = req.query.limit || 5;
    const properties = { limit: limit, offset: limit * page }; 
    const payload = req.body
    const data = await Create('peserta_didik_kesehatan', properties, payload)
    if (data.error) {
      return res.status(data.status).send(data.error);
    }
    res.send(data)
}
exports.gPeserta_didik_rekening = async (req, res) => {
    const page = req.query.page || 0;
    const limit = req.query.limit || 15;
    const properties = { limit: limit, offset: limit * page }; 

    const data = await findAll('peserta_didik_rekening', properties)
    res.send(data)
}
exports.pPeserta_didik_rekening = async (req, res) => {
    const page = req.query.page || 0;
    const limit = req.query.limit || 5;
    const properties = { limit: limit, offset: limit * page }; 
    const payload = req.body
    const data = await Create('peserta_didik_rekening', properties, payload)
    if (data.error) {
      return res.status(data.status).send(data.error);
    }
    res.send(data)
}
exports.pPeserta_didik_alamat = async (req, res) => {
    const page = req.query.page || 0;
    const limit = req.query.limit || 5;
    const properties = { limit: limit, offset: limit * page }; 
    const payload = req.body
    const data = await Create('peserta_didik_alamat', properties, payload)
    if (data.error) {
      return res.status(data.status).send(data.error);
    }
    res.send(data)
}
exports.pPeserta_didik_kontak = async (req, res) => {
    const page = req.query.page || 0;
    const limit = req.query.limit || 5;
    const properties = { limit: limit, offset: limit * page }; 
    const payload = req.body
    const data = await Create('peserta_didik_kontak', properties, payload)
    if (data.error) {
      return res.status(data.status).send(data.error);
    }
    res.send(data)
}
exports.upeserta_didik = async (req, res) => {
  const payload = req.body
  const id = req.params.id
  const properties = { where : { 'peserta_didik_id' : id }}; 
  
  const data = await Update('peserta_didik', payload, properties)
  res.send(data)
}
exports.upeserta_didik_alamat = async (req, res) => {
  const payload = req.body
  const id = req.params.id
  const properties = { where : { 'peserta_didik_alamat_id' : id }}; 
  
  const data = await Update('peserta_didik_alamat', payload, properties)
  res.send(data)
}
exports.upeserta_didik_kesehatan = async (req, res) => {
  const payload = req.body
  const id = req.params.id
  const properties = { where : { 'peserta_didik_kesehatan_id' : id }}; 
  
  const data = await Update('peserta_didik_kesehatan', payload, properties)
  res.send(data)
}
exports.upeserta_didik_kontak = async (req, res) => {
  const payload = req.body
  const id = req.params.id
  const properties = { where : { 'peserta_didik_kontak_id' : id }}; 
  
  const data = await Update('peserta_didik_kontak', payload, properties)
  res.send(data)
}
exports.upeserta_didik_rekening = async (req, res) => {
  const payload = req.body
  const id = req.params.id
  const properties = { where : { 'peserta_didik_rekening_id' : id }}; 
  
  const data = await Update('peserta_didik_rekening', payload, properties)
  res.send(data)
}
exports.dpeserta_didik = async (req, res) => {
  const id = req.params.id
  const properties = { where : { 'peserta_didik_id' : id }}; 

  const data = await Delete('peserta_didik', properties)
  res.send(data)
}
exports.dpeserta_didik_alamat = async (req, res) => {
  const id = req.params.id
  const properties = { where : { 'peserta_didik_alamat_id' : id }}; 

  const data = await Delete('peserta_didik_alamat', properties)
  res.send(data)
}
exports.dpeserta_didik_kesehatan = async (req, res) => {
  const id = req.params.id
  const properties = { where : { 'peserta_didik_kesehatan_id' : id }}; 

  const data = await Delete('peserta_didik_kesehatan', properties)
  res.send(data)
}
exports.dpeserta_didik_kontak = async (req, res) => {
  const id = req.params.id
  const properties = { where : { 'peserta_didik_kontak_id' : id }}; 

  const data = await Delete('peserta_didik_kontak', properties)
  res.send(data)
}
exports.dpeserta_didik_rekening = async (req, res) => {
  const id = req.params.id
  const properties = { where : { 'peserta_didik_rekening_id' : id }}; 

  const data = await Delete('peserta_didik_rekening', properties)
  res.send(data)
}
