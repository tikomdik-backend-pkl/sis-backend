const sequelize = require("../models");
const initModels = require("../models/init-models");

const models = initModels(sequelize);

const findAll = async (modelName, properties) => {
  try {
    const Model = models[modelName];

    let total_rows = await Model.count();
    let total_page = Math.ceil(total_rows / properties.limit);
    const data = await Model.findAll(properties);
    return { data: data, total_page: total_page, total_rows: total_rows, };
  } catch (err) {
    console.error(err);
    return `Server error, ${err}`;
  }
};
const Create = async (modelName, properties, payload) => {
  try {
    const Model = models[modelName];

    let total_rows = await Model.count();
    let total_page = Math.ceil(total_rows / properties.limit);
    const data = await Model.create(payload);
    return { data: data};
  } catch (err) {
    console.error(err);
    return { error: `Server error, ${err}`, status: 500 };
  }
};

const Delete = async (modelName, properties) => {
  try {
    const Model = models[modelName];

    const data = await Model.destroy(properties);
    return { data: data };
  } catch (err) {
    console.error(err);
    return `Server error, ${err}`;
  }
};

const Update = async (modelName, payload, properties) => {
  try {
     // properties = { where: {*nama kolumn* : *data*}, *property lain* }
     // ^^ Reminder bentuk variable properties untuk update ^^
    const Model = models[modelName];
    const data = await Model.update(payload, properties);
    return { data: data };
  } catch (err) {
    console.error(err);
    return `Server error, ${err}`;
  }
};


module.exports = { findAll, Create, Update, Delete };
