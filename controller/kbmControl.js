const {findAll, Create, Update, Delete} = require('.')



exports.gJadwal = async (req, res) => {
    const page = req.query.page || 0;
    const limit = req.query.limit || 5;
    const properties = { limit: limit, offset: limit * page }; 

    const data = await findAll('jadwal_kbm', properties)
    res.send(data)
}
exports.gMerdeka_capaian_sp = async (req, res) => {
    const page = req.query.page || 0;
    const limit = req.query.limit || 5;
    const properties = { limit: limit, offset: limit * page }; 

    const data = await findAll('merdeka_capaian_sp', properties)
    res.send(data)
}
exports.gMerdeka_elemen_sp = async (req, res) => {
    const page = req.query.page || 0;
    const limit = req.query.limit || 5;
    const properties = { limit: limit, offset: limit * page }; 

    const data = await findAll('merdeka_elemen_sp', properties)
    res.send(data)
}
exports.gPtk_mapel = async (req, res) => {
    const page = req.query.page || 0;
    const limit = req.query.limit || 5;
    const properties = { limit: limit, offset: limit * page }; 

    const data = await findAll('ptk_mapel', properties)
    res.send(data)
}
exports.gMapel_sp = async (req, res) => {
    const page = req.query.page || 0;
    const limit = req.query.limit || 5;
    const properties = { limit: limit, offset: limit * page }; 

    const data = await findAll('mapel_sp', properties)
    res.send(data)
}
exports.pJadwal = async (req, res) => {
    const page = req.query.page || 0;
    const limit = req.query.limit || 5;
    const properties = { limit: limit, offset: limit * page }; 
    const payload = req.body
    const data = await Create('jadwal_kbm', properties, payload)
    if (data.error) {
      return res.status(data.status).send(data.error);
    }
    res.send(data)
}
exports.pMerdeka_capaian_sp = async (req, res) => {
    const page = req.query.page || 0;
    const limit = req.query.limit || 5;
    const properties = { limit: limit, offset: limit * page }; 
    const payload = req.body
    const data = await Create('merdeka_capaian_sp', properties, payload)
    if (data.error) {
      return res.status(data.status).send(data.error);
    }
    res.send(data)
}
exports.pMerdeka_elemen_sp = async (req, res) => {
    const page = req.query.page || 0;
    const limit = req.query.limit || 5;
    const properties = { limit: limit, offset: limit * page }; 
    const payload = req.body
    const data = await Create('merdeka_elemen_sp', properties, payload)
    if (data.error) {
      return res.status(data.status).send(data.error);
    }
    res.send(data)
}
exports.pPtk_mapel = async (req, res) => {
    const page = req.query.page || 0;
    const limit = req.query.limit || 5;
    const properties = { limit: limit, offset: limit * page }; 
    const payload = req.body
    const data = await Create('ptk_mapel', properties, payload)
    if (data.error) {
      return res.status(data.status).send(data.error);
    }
    res.send(data)
}
exports.pMapel_sp = async (req, res) => {
    const page = req.query.page || 0;
    const limit = req.query.limit || 5;
    const properties = { limit: limit, offset: limit * page }; 
    const payload = req.body
    const data = await Create('mapel_sp', properties, payload)
    if (data.error) {
      return res.status(data.status).send(data.error);
    }
    res.send(data)
}
exports.djadwal_kbm = async (req, res) => {
  const id = req.params.id
  const properties = { where : { 'jadwal_kbm_id' : id }}; 

  const data = await Delete('jadwal_kbm', properties)
  res.send(data)
}
exports.dmapel_sp = async (req, res) => {
  const id = req.params.id
  const properties = { where : { 'mapel_sp_id' : id }}; 

  const data = await Delete('mapel_sp', properties)
  res.send(data)
}
exports.dmerdeka_capaian_sp = async (req, res) => {
  const id = req.params.id
  const properties = { where : { 'capaian_id' : id }}; 

  const data = await Delete('merdeka_capaian_sp', properties)
  res.send(data)
}
exports.dmerdeka_elemen_sp = async (req, res) => {
  const id = req.params.id
  const properties = { where : { 'elemen_id' : id }}; 

  const data = await Delete('merdeka_elemen_sp', properties)
  res.send(data)
}
exports.dptk_mapel = async (req, res) => {
  const id = req.params.id
  const properties = { where : { 'ptk_mapel_id' : id }}; 

  const data = await Delete('ptk_mapel', properties)
  res.send(data)
}
exports.ujadwal_kbm = async (req, res) => {
  const payload = req.body
  const id = req.params.id
  const properties = { where : { 'jadwal_kbm_id' : id }}; 
  const data = await Update('jadwal_kbm', payload, properties)
  res.send(data)
}
exports.umapel_sp = async (req, res) => {
  const payload = req.body
  const id = req.params.id
  const properties = { where : { 'mapel_sp_id' : id }}; 
  const data = await Update('mapel_sp', payload, properties)
  res.send(data)
}
exports.umerdeka_capaian_sp = async (req, res) => {
  const payload = req.body
  const id = req.params.id
  const properties = { where : { 'capaian_id' : id }}; 
  const data = await Update('merdeka_capaian_sp', payload, properties)
  res.send(data)
}
exports.umerdeka_elemen_sp = async (req, res) => {
  const payload = req.body
  const id = req.params.id
  const properties = { where : { 'elemen_id' : id }}; 
  const data = await Update('merdeka_elemen_sp', payload, properties)
  res.send(data)
}
exports.uptk_mapel = async (req, res) => {
  const payload = req.body
  const id = req.params.id
  const properties = { where : { 'ptk_mapel_id' : id }}; 
  const data = await Update('ptk_mapel', payload, properties)
  res.send(data)
}
