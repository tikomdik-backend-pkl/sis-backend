const {findAll} = require(".");



exports.gAgama = async (req, res) => {
    const page = req.query.page || 0;
    const limit = req.query.limit || 5;
    const properties = { limit: limit, offset: limit * page }; 

    const data = await findAll('agama', properties)
    res.send(data)
}
exports.gAkreditasi = async (req, res) => {
    const page = req.query.page || 0;
    const limit = req.query.limit || 5;
    const properties = { limit: limit, offset: limit * page }; 

    const data = await findAll('akreditasi', properties)
    res.send(data)
}
exports.gAlasan_layak_pip = async (req, res) => {
    const page = req.query.page || 0;
    const limit = req.query.limit || 5;
    const properties = { limit: limit, offset: limit * page }; 

    const data = await findAll('alasan_layak_pip', properties)
    res.send(data)
}
exports.gAlat_transportasi = async (req, res) => {
    const page = req.query.page || 0;
    const limit = req.query.limit || 5;
    const properties = { limit: limit, offset: limit * page }; 

    const data = await findAll('alat_transportasi', properties)
    res.send(data)
}
exports.gBank = async (req, res) => {
    const page = req.query.page || 0;
    const limit = req.query.limit || 5;
    const properties = { limit: limit, offset: limit * page }; 

    const data = await findAll('bank', properties)
    res.send(data)
}
exports.gBentuk_pendidikan = async (req, res) => {
    const page = req.query.page || 0;
    const limit = req.query.limit || 5;
    const properties = { limit: limit, offset: limit * page }; 

    const data = await findAll('bentuk_pendidikan', properties)
    res.send(data)
}
exports.gGelar_akademik = async (req, res) => {
    const page = req.query.page || 0;
    const limit = req.query.limit || 5;
    const properties = { limit: limit, offset: limit * page }; 

    const data = await findAll('gelar_akademik', properties)
    res.send(data)
}
exports.gHari = async (req, res) => {
    const page = req.query.page || 0;
    const limit = req.query.limit || 5;
    const properties = { limit: limit, offset: limit * page }; 

    const data = await findAll('hari', properties)
    res.send(data)
}
exports.gJabatan_tugas_ptk = async (req, res) => {
    const page = req.query.page || 0;
    const limit = req.query.limit || 5;
    const properties = { limit: limit, offset: limit * page }; 

    const data = await findAll('jabatan_tugas_ptk', properties)
    res.send(data)
}
exports.gJenis_kewarganegaraan = async (req, res) => {
    const page = req.query.page || 0;
    const limit = req.query.limit || 5;
    const properties = { limit: limit, offset: limit * page }; 

    const data = await findAll('jenis_kewarganegaraan', properties)
    res.send(data)
}
exports.gJenis_pendaftaran = async (req, res) => {
    const page = req.query.page || 0;
    const limit = req.query.limit || 5;
    const properties = { limit: limit, offset: limit * page }; 

    const data = await findAll('jenis_pendaftaran', properties)
    res.send(data)
}
exports.gJenis_ptk = async (req, res) => {
    const page = req.query.page || 0;
    const limit = req.query.limit || 5;
    const properties = { limit: limit, offset: limit * page }; 

    const data = await findAll('jenis_ptk', properties)
    res.send(data)
}
exports.gJenis_rombel = async (req, res) => {
    const page = req.query.page || 0;
    const limit = req.query.limit || 5;
    const properties = { limit: limit, offset: limit * page }; 

    const data = await findAll('jenis_rombel', properties)
    res.send(data)
}
exports.gJenis_tinggal = async (req, res) => {
    const page = req.query.page || 0;
    const limit = req.query.limit || 5;
    const properties = { limit: limit, offset: limit * page }; 

    const data = await findAll('jenis_tinggal', properties)
    res.send(data)
}
exports.gJenjang_pendidikan = async (req, res) => {
    const page = req.query.page || 0;
    const limit = req.query.limit || 5;
    const properties = { limit: limit, offset: limit * page }; 

    const data = await findAll('jenjang_pendidikan', properties)
    res.send(data)
}
exports.gJurusan = async (req, res) => {
    const page = req.query.page || 0;
    const limit = req.query.limit || 5;
    const properties = { limit: limit, offset: limit * page }; 

    const data = await findAll('jurusan', properties)
    res.send(data)
}
exports.gKeahlian_laboratorium = async (req, res) => {
    const page = req.query.page || 0;
    const limit = req.query.limit || 5;
    const properties = { limit: limit, offset: limit * page }; 

    const data = await findAll('keahlian_laboratorium', properties)
    res.send(data)
}
exports.gLembaga_pengangkat = async (req, res) => {
    const page = req.query.page || 0;
    const limit = req.query.limit || 5;
    const properties = { limit: limit, offset: limit * page }; 

    const data = await findAll('lembaga_pengangkat', properties)
    res.send(data)
}
exports.gMerdeka_capaian = async (req, res) => {
    const page = req.query.page || 0;
    const limit = req.query.limit || 5;
    const properties = { limit: limit, offset: limit * page }; 

    const data = await findAll('merdeka_capaian', properties)
    res.send(data)
}
exports.gMerdeka_elemen = async (req, res) => {
    const page = req.query.page || 0;
    const limit = req.query.limit || 5;
    const properties = { limit: limit, offset: limit * page }; 

    const data = await findAll('merdeka_elemen', properties)
    res.send(data)
}
exports.gMerdeka_mapel = async (req, res) => {
    const page = req.query.page || 0;
    const limit = req.query.limit || 5;
    const properties = { limit: limit, offset: limit * page }; 

    const data = await findAll('merdeka_mapel', properties)
    res.send(data)
}
exports.gPangkat_golongan = async (req, res) => {
    const page = req.query.page || 0;
    const limit = req.query.limit || 5;
    const properties = { limit: limit, offset: limit * page }; 

    const data = await findAll('pangkat_golongan', properties)
    res.send(data)
}
exports.gPekerjaan = async (req, res) => {
    const page = req.query.page || 0;
    const limit = req.query.limit || 5;
    const properties = { limit: limit, offset: limit * page }; 

    const data = await findAll('pekerjaan', properties)
    res.send(data)
}
exports.gProgram_studi = async (req, res) => {
    const page = req.query.page || 0;
    const limit = req.query.limit || 5;
    const properties = { limit: limit, offset: limit * page }; 

    const data = await findAll('program_studi', properties)
    res.send(data)
}
exports.gRef_sekolah = async (req, res) => {
    const page = req.query.page || 0;
    const limit = req.query.limit || 5;
    const properties = { limit: limit, offset: limit * page }; 

    const data = await findAll('ref_sekolah', properties)
    res.send(data)
}
exports.gSemester = async (req, res) => {
    const page = req.query.page || 0;
    const limit = req.query.limit || 5;
    const properties = { limit: limit, offset: limit * page }; 

    const data = await findAll('semester', properties)
    res.send(data)
}
exports.gSertifikasi_iso = async (req, res) => {
    const page = req.query.page || 0;
    const limit = req.query.limit || 5;
    const properties = { limit: limit, offset: limit * page }; 

    const data = await findAll('sertifikasi_iso', properties)
    res.send(data)
}
exports.gStatus_kepegawaian = async (req, res) => {
    const page = req.query.page || 0;
    const limit = req.query.limit || 5;
    const properties = { limit: limit, offset: limit * page }; 

    const data = await findAll('status_kepegawaian', properties)
    res.send(data)
}
exports.gStatus_kepemilikan = async (req, res) => {
    const page = req.query.page || 0;
    const limit = req.query.limit || 5;
    const properties = { limit: limit, offset: limit * page }; 

    const data = await findAll('status_kepemilikan', properties)
    res.send(data)
}
exports.gStatus_perkawinan = async (req, res) => {
    const page = req.query.page || 0;
    const limit = req.query.limit || 5;
    const properties = { limit: limit, offset: limit * page }; 

    const data = await findAll('status_perkawinan', properties)
    res.send(data)
}
exports.gSumber_gaji = async (req, res) => {
    const page = req.query.page || 0;
    const limit = req.query.limit || 5;
    const properties = { limit: limit, offset: limit * page }; 

    const data = await findAll('sumber_gaji', properties)
    res.send(data)
}
exports.gTahun_ajaran = async (req, res) => {
    const page = req.query.page || 0;
    const limit = req.query.limit || 5;
    const properties = { limit: limit, offset: limit * page }; 

    const data = await findAll('tahun_ajaran', properties)
    res.send(data)
}
exports.gTingkat_pendidikan = async (req, res) => {
    const page = req.query.page || 0;
    const limit = req.query.limit || 5;
    const properties = { limit: limit, offset: limit * page }; 

    const data = await findAll('tingkat_pendidikan', properties)
    res.send(data)
}
exports.gWaktu_penyelenggaraan = async (req, res) => {
    const page = req.query.page || 0;
    const limit = req.query.limit || 5;
    const properties = { limit: limit, offset: limit * page }; 

    const data = await findAll('waktu_penyelenggaraan', properties)
    res.send(data)
}
exports.gWilayah_kemendagri_2022 = async (req, res) => {
    const page = req.query.page || 0;
    const limit = req.query.limit || 5;
    const properties = { limit: limit, offset: limit * page }; 

    const data = await findAll('wilayah_kemendagri_2022', properties)
    res.send(data)
}