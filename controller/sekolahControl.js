const {findAll, Create, Update, Delete} = require('.')
exports.gAkreditasi = async (req, res) => {
    const page = req.query.page || 0;
    const limit = req.query.limit || 5;
    const properties = { limit: limit, offset: limit * page }; 

    const data = await findAll('sekolah_akreditasi', properties)
    res.send(data)
}
exports.gSekolah_alamat = async (req, res) => {
    const page = req.query.page || 0;
    const limit = req.query.limit || 5;
    const properties = { limit: limit, offset: limit * page }; 

    const data = await findAll('sekolah_alamat', properties)
    res.send(data)
}
exports.gSekolah_bank = async (req, res) => {
    const page = req.query.page || 0;
    const limit = req.query.limit || 5;
    const properties = { limit: limit, offset: limit * page }; 

    const data = await findAll('sekolah_bank', properties)
    res.send(data)
}
exports.gSekolah_identitas = async (req, res) => {
    const page = req.query.page || 0;
    const limit = req.query.limit || 5;
    const properties = { limit: limit, offset: limit * page }; 

    const data = await findAll('sekolah_identitas', properties)
    res.send(data)
}
exports.gSekolah_kepemilikan = async (req, res) => {
    const page = req.query.page || 0;
    const limit = req.query.limit || 5;
    const properties = { limit: limit, offset: limit * page }; 

    const data = await findAll('sekolah_kepemilikan', properties)
    res.send(data)
}
exports.gSekolah_sertifikasi_iso = async (req, res) => {
    const page = req.query.page || 0;
    const limit = req.query.limit || 5;
    const properties = { limit: limit, offset: limit * page }; 

    const data = await findAll('sekolah_sertifikasi_iso', properties)
    res.send(data)
}
exports.pSekolah_akreditasi = async (req, res) => {
    const page = req.query.page || 0;
    const limit = req.query.limit || 5;
    const properties = { limit: limit, offset: limit * page }; 
    const payload = req.body
    const data = await Create('sekolah_akreditasi', properties, payload)
    if (data.error) {
      return res.status(data.status).send(data.error);
    }
    res.send(data)
}
exports.pSekolah_alamat = async (req, res) => {
    const page = req.query.page || 0;
    const limit = req.query.limit || 5;
    const properties = { limit: limit, offset: limit * page }; 
    const payload = req.body
    const data = await Create('sekolah_alamat', properties, payload)
    if (data.error) {
      return res.status(data.status).send(data.error);
    }
    res.send(data)
}
exports.pSekolah_bank = async (req, res) => {
    const page = req.query.page || 0;
    const limit = req.query.limit || 5;
    const properties = { limit: limit, offset: limit * page }; 
    const payload = req.body
    const data = await Create('sekolah_bank', properties, payload)
    if (data.error) {
      return res.status(data.status).send(data.error);
    }
    res.send(data)
}
exports.pSekolah_identitas = async (req, res) => {
    const page = req.query.page || 0;
    const limit = req.query.limit || 5;
    const properties = { limit: limit, offset: limit * page }; 
    const payload = req.body
    const data = await Create('sekolah_identitas', properties, payload)
    if (data.error) {
      return res.status(data.status).send(data.error);
    }
    res.send(data)
}
exports.pSekolah_kepemilikan = async (req, res) => {
    const page = req.query.page || 0;
    const limit = req.query.limit || 5;
    const properties = { limit: limit, offset: limit * page }; 
    const payload = req.body
    const data = await Create('sekolah_kepemilikan', properties, payload)
    if (data.error) {
      return res.status(data.status).send(data.error);
    }
    res.send(data)
}
exports.pSekolah_sertifikasi_iso = async (req, res) => {
    const page = req.query.page || 0;
    const limit = req.query.limit || 5;
    const properties = { limit: limit, offset: limit * page }; 
    const payload = req.body
    const data = await Create('sekolah_sertifikasi_iso', properties, payload)
    if (data.error) {
      return res.status(data.status).send(data.error);
    }
    res.send(data)
}
exports.usekolah_akreditasi = async (req, res) => {
  const payload = req.body
  const id = req.params.id
  const properties = { where : { 'sekolah_akreditasi_id' : id }}; 
  
  const data = await Update('sekolah_akreditasi', payload, properties)
  res.send(data)
}
exports.usekolah_alamat = async (req, res) => {
  const payload = req.body
  const id = req.params.id
  const properties = { where : { 'sekolah_alamat_id' : id }}; 
  
  const data = await Update('sekolah_alamat', payload, properties)
  res.send(data)
}
exports.usekolah_bank = async (req, res) => {
  const payload = req.body
  const id = req.params.id
  const properties = { where : { 'sekolah_bank_id' : id }}; 
  
  const data = await Update('sekolah_bank', payload, properties)
  res.send(data)
}
exports.usekolah_identitas = async (req, res) => {
  const payload = req.body
  const id = req.params.id
  const properties = { where : { 'sekolah_id' : id }}; 
  
  const data = await Update('sekolah_identitas', payload, properties)
  res.send(data)
}
exports.usekolah_kepemilikan = async (req, res) => {
  const payload = req.body
  const id = req.params.id
  const properties = { where : { 'sekolah_kepemilikan_id' : id }}; 
  
  const data = await Update('sekolah_kepemilikan', payload, properties)
  res.send(data)
}
exports.usekolah_sertifikasi_iso = async (req, res) => {
  const payload = req.body
  const id = req.params.id
  const properties = { where : { 'sekolah_sertifikasi_iso_id' : id }}; 
  
  const data = await Update('sekolah_sertifikasi_iso', payload, properties)
  res.send(data)
}
exports.dsekolah_akreditasi = async (req, res) => {
  const id = req.params.id
  const properties = { where : { 'sekolah_akreditasi_id' : id }}; 

  const data = await Delete('sekolah_akreditasi', properties)
  res.send(data)
}
exports.dsekolah_alamat = async (req, res) => {
  const id = req.params.id
  const properties = { where : { 'sekolah_alamat_id' : id }}; 

  const data = await Delete('sekolah_alamat', properties)
  res.send(data)
}
exports.dsekolah_bank = async (req, res) => {
  const id = req.params.id
  const properties = { where : { 'sekolah_bank_id' : id }}; 

  const data = await Delete('sekolah_bank', properties)
  res.send(data)
}
exports.dsekolah_identitas = async (req, res) => {
  const id = req.params.id
  const properties = { where : { 'sekolah_id' : id }}; 

  const data = await Delete('sekolah_identitas', properties)
  res.send(data)
}
exports.dsekolah_kepemilikan = async (req, res) => {
  const id = req.params.id
  const properties = { where : { 'sekolah_kepemilikan_id' : id }}; 

  const data = await Delete('sekolah_kepemilikan', properties)
  res.send(data)
}
exports.dsekolah_sertifikasi_iso = async (req, res) => {
  const id = req.params.id
  const properties = { where : { 'sekolah_sertifikasi_iso_id' : id }}; 

  const data = await Delete('sekolah_sertifikasi_iso', properties)
  res.send(data)
}
