const {findAll, Create, Update, Delete} = require('.')

exports.gTugas_mengajar = async (req, res) => {
    const page = req.query.page || 0;
    const limit = req.query.limit || 5;
    const properties = { limit: limit, offset: limit * page }; 

    const data = await findAll('ptk_tugas_mengajar', properties)
    res.send(data)
}
exports.gPtk = async (req, res) => {
    const page = req.query.page || 0;
    const limit = req.query.limit || 5;
    const properties = { limit: limit, offset: limit * page }; 

    const data = await findAll('ptk', properties)
    res.send(data)
}
exports.gPtk_pangkat_gol = async (req, res) => {
    const page = req.query.page || 0;
    const limit = req.query.limit || 5;
    const properties = { limit: limit, offset: limit * page }; 

    const data = await findAll('ptk_pangkat_gol', properties)
    res.send(data)
}
exports.gPtk_pend_formal = async (req, res) => {
    const page = req.query.page || 0;
    const limit = req.query.limit || 5;
    const properties = { limit: limit, offset: limit * page }; 

    const data = await findAll('ptk_pend_formal', properties)
    res.send(data)
}
exports.gPtk_temp = async (req, res) => {
    const page = req.query.page || 0;
    const limit = req.query.limit || 5;
    const properties = { limit: limit, offset: limit * page }; 

    const data = await findAll('ptk_temp', properties)
    res.send(data)
}
exports.pPtk_pangkat_gol = async (req, res) => {
    const page = req.query.page || 0;
    const limit = req.query.limit || 5;
    const properties = { limit: limit, offset: limit * page }; 
    const payload = req.body

    const data = await Create('ptk_pangkat_gol', properties, payload)
    if (data.error) {
        return res.status(data.status).send(data.error);
      }
    res.send(data)
}
exports.pPtk_pend_formal = async (req, res) => {
    const page = req.query.page || 0;
    const limit = req.query.limit || 5;
    const properties = { limit: limit, offset: limit * page }; 
    const payload = req.body

    const data = await Create('ptk_pend_formal', properties, payload)
    if (data.error) {
        return res.status(data.status).send(data.error);
      }
    res.send(data)
}
exports.pPtk_temp = async (req, res) => {
    const page = req.query.page || 0;
    const limit = req.query.limit || 5;
    const properties = { limit: limit, offset: limit * page }; 
    const payload = req.body

    const data = await Create('ptk_temp', properties, payload)
    if (data.error) {
        return res.status(data.status).send(data.error);
      }
    res.send(data)
}
exports.pPtk = async (req, res) => {
    const page = req.query.page || 0;
    const limit = req.query.limit || 5;
    const properties = { limit: limit, offset: limit * page }; 
    const payload = req.body
    const data = await Create('ptk', properties, payload)
    if (data.error) {
        return res.status(data.status).send(data.error);
      }
    res.send(data)
}

exports.pTugas_mengajar = async (req, res) => {
    const page = req.query.page || 0;
    const limit = req.query.limit || 5;
    const properties = { limit: limit, offset: limit * page }; 
    const payload = req.body
    const data = await Create('ptk_tugas_mengajar', properties, payload)
    if (data.error) {
        return res.status(data.status).send(data.error);
      }
    res.send(data)
}
exports.dptk = async (req, res) => {
    const id = req.params.id
    const properties = { where : { 'ptk_id' : id }}; 

    const data = await Delete('ptk', properties)
    res.send(data)
}
exports.dptk_pangkat_gol = async (req, res) => {
    const id = req.params.id
    const properties = { where : { 'ptk_pangkat_gol_id' : id }}; 

    const data = await Delete('ptk_pangkat_gol', properties)
    res.send(data)
}
exports.dptk_pend_formal = async (req, res) => {
    const id = req.params.id
    const properties = { where : { 'ptk_pend_formal_id' : id }}; 

    const data = await Delete('ptk_pend_formal', properties)
    res.send(data)
}
exports.dptk_temp = async (req, res) => {
    const id = req.params.id
    const properties = { where : { 'ptk_temp_id' : id }}; 

    const data = await Delete('ptk_temp', properties)
    res.send(data)
}
exports.dptk_tugas_mengajar = async (req, res) => {
    const id = req.params.id
    const properties = { where : { 'ptk_penugasan_id' : id }}; 

    const data = await Delete('ptk_tugas_mengajar', properties)
    res.send(data)
}
exports.uptk = async (req, res) => {
    const payload = req.body
    const properties = { where : { 'ptk_id' : id }}; 
    
    const data = await Update('ptk', payload, properties)
    res.send(data)
}
exports.uptk_pangkat_gol = async (req, res) => {
    const payload = req.body
    const properties = { where : { 'ptk_pangkat_gol_id' : id }}; 
    
    const data = await Update('ptk_pangkat_gol', payload, properties)
    res.send(data)
}
exports.uptk_pend_formal = async (req, res) => {
    const payload = req.body
    const properties = { where : { 'ptk_pend_formal_id' : id }}; 
    
    const data = await Update('ptk_pend_formal', payload, properties)
    res.send(data)
}
exports.uptk_temp = async (req, res) => {
    const payload = req.body
    const properties = { where : { 'ptk_temp_id' : id }}; 
    
    const data = await Update('ptk_temp', payload, properties)
    res.send(data)
}
exports.uptk_tugas_mengajar = async (req, res) => {
    const payload = req.body
    const properties = { where : { 'ptk_penugasan_id' : id }}; 
    
    const data = await Update('ptk_tugas_mengajar', payload, properties)
    res.send(data)
}

