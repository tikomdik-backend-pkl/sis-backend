const express = require("express");
const router = express.Router();
const {
  gAgama,
  gAkreditasi,
  gAlasan_layak_pip,
  gAlat_transportasi,
  gBank,
  gBentuk_pendidikan,
  gGelar_akademik,
  gHari,
  gJabatan_tugas_ptk,
  gJenis_kewarganegaraan,
  gJenis_pendaftaran,
  gJenis_ptk,
  gJenis_rombel,
  gJenis_tinggal,
  gJenjang_pendidikan,
  gJurusan,
  gKeahlian_laboratorium,
  gLembaga_pengangkat,
  gMerdeka_capaian,
  gMerdeka_elemen,
  gMerdeka_mapel,
  gPangkat_golongan,
  gPekerjaan,
  gProgram_studi,
  gRef_sekolah,
  gSemester,
  gSertifikasi_iso,
  gStatus_kepegawaian,
  gStatus_kepemilikan,
  gStatus_perkawinan,
  gSumber_gaji,
  gTahun_ajaran,
  gTingkat_pendidikan,
  gWaktu_penyelenggaraan,
  gWilayah_kemendagri_2022,
  
} = require("../controller/refControl");

router.get("/agama", gAgama)
.get("/akreditasi", gAkreditasi)
.get("/alasan_layak_pip", gAlasan_layak_pip)
.get("/alat_transportasi", gAlat_transportasi)
.get("/bank", gBank)
.get("/bentuk_pendidikan", gBentuk_pendidikan)
.get("/gelar_akademik", gGelar_akademik)
.get("/hari", gHari)
.get("/jabatan_tugas_ptk", gJabatan_tugas_ptk)
.get("/jenis_kewarganegaraan", gJenis_kewarganegaraan)
.get("/jenis_pendaftaran", gJenis_pendaftaran)
.get("/jenis_ptk", gJenis_ptk)
.get("/jenis_rombel", gJenis_rombel)
.get("/jenis_tinggal", gJenis_tinggal)
.get("/jenjang_pendidikan", gJenjang_pendidikan)
.get("/jurusan", gJurusan)
.get("/keahlian_laboratorium", gKeahlian_laboratorium)
.get("/lembaga_pengangkat", gLembaga_pengangkat)
.get("/merdeka_capaian", gMerdeka_capaian)
.get("/merdeka_elemen", gMerdeka_elemen)
.get("/merdeka_mapel", gMerdeka_mapel)
.get("/pangkat_golongan", gPangkat_golongan)
.get("/pekerjaan", gPekerjaan)
.get("/program_studi", gProgram_studi)
.get("/ref_sekolah", gRef_sekolah)
.get("/semester", gSemester)
.get("/sertifikasi_iso", gSertifikasi_iso)
.get("/status_kepegawaian", gStatus_kepegawaian)
.get("/status_kepemilikan", gStatus_kepemilikan)
.get("/status_perkawinan", gStatus_perkawinan)
.get("/sumber_gaji", gSumber_gaji)
.get("/tahun_ajaran", gTahun_ajaran)
.get("/tingkat_pendidikan", gTingkat_pendidikan)
.get("/waktu_penyelenggaraan", gWaktu_penyelenggaraan)
.get("/wilayah_kemendagri_2022", gWilayah_kemendagri_2022)

module.exports = router;
