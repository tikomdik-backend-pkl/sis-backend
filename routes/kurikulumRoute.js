const express = require("express");
const { gKurikulum, gRombel, gAnggota_rombel, gProgram, pKurikulum, pAnggota_rombel, pProgram, pRombel, ukurikulum, ukurikulum_rombongan_belajar, ukurikulum_anggota_rombel, ukurikulum_program, dkurikulum_program, dkurikulum_rombongan_belajar, dkurikulum_anggota_rombel, dkurikulum } = require("../controller/kurikulumControl");
const router = express.Router()

router.get("/", gKurikulum)
router.post("/", pKurikulum)
router.patch("/:id", ukurikulum)
router.delete("/:id", dkurikulum)
router.get('/rombel', gRombel)
router.post('/rombel', pRombel)
router.patch('/rombel/:id', ukurikulum_rombongan_belajar)
router.delete('/rombel/:id', dkurikulum_rombongan_belajar)
router.get('/anggota_rombel', gAnggota_rombel)
router.post('/anggota_rombel', pAnggota_rombel)
router.patch('/anggota_rombel/:id', ukurikulum_anggota_rombel)
router.delete('/anggota_rombel/:id', dkurikulum_anggota_rombel)
router.get('/program', gProgram)
router.post('/program', pProgram)
router.patch('/program/:id', ukurikulum_program)
router.delete('/program/:id', dkurikulum_program)

module.exports = router