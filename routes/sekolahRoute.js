const express = require("express");
const { gSekolah_alamat, gSekolah_bank, gSekolah_identitas, gSekolah_kepemilikan,gSekolah_sertifikasi_iso, gAkreditasi,  pSekolah_akreditasi, pSekolah_alamat, pSekolah_bank, pSekolah_identitas, pSekolah_kepemilikan, pSekolah_sertifikasi_iso, dsekolah_alamat, dsekolah_bank, dsekolah_identitas, dsekolah_kepemilikan, dsekolah_sertifikasi_iso, dsekolah_akreditasi, usekolah_akreditasi, usekolah_alamat, usekolah_bank, usekolah_identitas, usekolah_kepemilikan, usekolah_sertifikasi_iso } = require("../controller/sekolahControl");

const router = express.Router();

router
  .get("/akreditasi", gAkreditasi)
  .get("/alamat", gSekolah_alamat)
  .get("/bank", gSekolah_bank)
  .get("/identitas", gSekolah_identitas)
  .get("/kepemilikan", gSekolah_kepemilikan)
  .get("/sertifikasi_iso", gSekolah_sertifikasi_iso)
  .post("/akreditasi", pSekolah_akreditasi)
  .post("/alamat", pSekolah_alamat)
  .post("/bank", pSekolah_bank)
  .post("/identitas", pSekolah_identitas)
  .post("/kepemilikan", pSekolah_kepemilikan)
  .post("/sertifikasi_iso", pSekolah_sertifikasi_iso)
  .delete("/akreditasi/:id", dsekolah_akreditasi)
  .delete("/alamat/:id", dsekolah_alamat)
  .delete("/bank/:id", dsekolah_bank)
  .delete("/identitas/:id", dsekolah_identitas)
  .delete("/kepemilikan/:id", dsekolah_kepemilikan)
  .delete("/sertifikasi_iso/:id", dsekolah_sertifikasi_iso)
  .patch("/akreditasi/:id", usekolah_akreditasi)
  .patch("/alamat/:id", usekolah_alamat)
  .patch("/bank/:id", usekolah_bank)
  .patch("/identitas/:id", usekolah_identitas)
  .patch("/kepemilikan/:id", usekolah_kepemilikan)
  .patch("/sertifikasi_iso/:id", usekolah_sertifikasi_iso)

module.exports = router;