const express = require("express");
const {
  gJadwal,
  gMerdeka_capaian_sp,
  gMerdeka_elemen_sp,
  pJadwal,
  pMerdeka_capaian_sp,
  pMerdeka_elemen_sp,
  pPtk_mapel,
  pMapel_sp,
  gMapel_sp,
  gPtk_mapel,
  djadwal_kbm,
  dmerdeka_capaian_sp,
  dmerdeka_elemen_sp,
  dmapel_sp,
  dptk_mapel,
  ujadwal_kbm,
  umerdeka_capaian_sp,
  umerdeka_elemen_sp,
  umapel_sp,
  uptk_mapel,
} = require("../controller/kbmControl");
const router = express.Router();

router
  .get("/jadwal", gJadwal)
  .get("/capaian", gMerdeka_capaian_sp)
  .get("/elemen", gMerdeka_elemen_sp)
  .get("/mapel_sp", gMapel_sp)
  .get("/mapel_ptk", gPtk_mapel)
  .post("/jadwal", pJadwal)
  .post("/capaian", pMerdeka_capaian_sp)
  .post("/elemen", pMerdeka_elemen_sp)
  .post("/mapel_sp", pMapel_sp)
  .post("/mapel_ptk", pPtk_mapel)
  .delete("/jadwal/:id", djadwal_kbm)
  .delete("/capaian/:id", dmerdeka_capaian_sp)
  .delete("/elemen/:id", dmerdeka_elemen_sp)
  .delete("/mapel_sp/:id", dmapel_sp)
  .delete("/mapel_ptk/:id", dptk_mapel)
  .patch("/jadwal/:id", ujadwal_kbm)
  .patch("/capaian/:id", umerdeka_capaian_sp)
  .patch("/elemen/:id", umerdeka_elemen_sp)
  .patch("/mapel_sp/:id", umapel_sp)
  .patch("/mapel_ptk/:id", uptk_mapel)

module.exports = router;
