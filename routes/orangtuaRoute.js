const express = require("express");
const { gAlamat, gPekerjaan, pAlamat, dAlamat, uAlamat, gpenghasilan, dpenghasilan, upenghasilan, ppenghasilan } = require("../controller/orangtuaControl");
const router = express.Router()

router.get('/alamat', gAlamat)
router.post('/alamat', pAlamat)
router.patch('/alamat/:id', uAlamat)
router.delete('/alamat/:id', dAlamat)
router.get('/penghasilan', gpenghasilan)
router.post('/penghasilan', ppenghasilan)
router.patch('/penghasilan/:id', upenghasilan)
router.delete('/penghasilan/:id', dpenghasilan)

module.exports = router