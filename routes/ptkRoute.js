const express = require("express");
const { gPtk, gTugas_mengajar, gPtk_temp, gPtk_pend_formal, gPtk_pangkat_gol, pPtk_temp, pPtk_pend_formal, pPtk_pangkat_gol, uptk_tugas_mengajar, uptk, uptk_pangkat_gol, uptk_pend_formal, uptk_temp, dptk_tugas_mengajar, dptk, dptk_pangkat_gol, dptk_pend_formal, dptk_temp, pPtk, pTugas_mengajar } = require("../controller/ptkControl");
const router = express.Router()

router.get('/', gPtk)
      .get('/tugas_mengajar', gTugas_mengajar)
      .get('/temp', gPtk_temp)
      .get('/pend_formal', gPtk_pend_formal)
      .get('/pangkat_gol', gPtk_pangkat_gol)
      .post('/temp', pPtk_temp)
      .post('/pend_formal', pPtk_pend_formal)
      .post('/pangkat_gol', pPtk_pangkat_gol)
      .post('/', pPtk)
      .post('/tugas_mengajar', pTugas_mengajar)
      .delete('/temp/:id', dptk_temp)
      .delete('/pend_formal/:id', dptk_pend_formal)
      .delete('/pangkat_gol/:id', dptk_pangkat_gol)
      .delete('/:id', dptk)
      .delete('/tugas_mengajar/:id', dptk_tugas_mengajar)
      .patch('/temp/:id', uptk_temp)
      .patch('/pend_formal/:id', uptk_pend_formal)
      .patch('/pangkat_gol/:id', uptk_pangkat_gol)
      .patch('/:id', uptk)
      .patch('/tugas_mengajar/:id', uptk_tugas_mengajar)

module.exports = router;