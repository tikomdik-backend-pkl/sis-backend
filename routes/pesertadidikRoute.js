const express = require("express");
const { gPeserta_didik, gAlamat, gInklusi, gKontak_kawat, gKontak_siber, pPeserta_didik, pPeserta_didik_alamat, pPeserta_didik_inklusi, pPeserta_didik_kontak_kawat, pPeserta_didik_kontak_siber, pPeserta_didik_kontak, gPeserta_didik_kesehatan, gPeserta_didik_rekening, pPeserta_didik_rekening, pPeserta_didik_kesehatan, gKontak, dpeserta_didik_alamat, dpeserta_didik_kontak, dpeserta_didik_kesehatan, dpeserta_didik_rekening, dpeserta_didik, upeserta_didik_alamat, upeserta_didik_kontak, upeserta_didik_kesehatan, upeserta_didik_rekening, upeserta_didik } = require("../controller/peserta_didikControl");

const router = express.Router()

router.get('/', gPeserta_didik)
      .get('/alamat', gAlamat)
      .get('/kontak', gKontak)
      .get('/rekening', gPeserta_didik_rekening)
      .get('/kesehatan', gPeserta_didik_kesehatan)
      .post('/alamat', pPeserta_didik_alamat)
      .post('/kontak', pPeserta_didik_kontak)
      .post('/kesehatan', pPeserta_didik_kesehatan)
      .post('/rekening', pPeserta_didik_rekening)
      .post('/', pPeserta_didik)
      .delete('/alamat/:id', dpeserta_didik_alamat)
      .delete('/kontak/:id', dpeserta_didik_kontak)
      .delete('/kesehatan/:id', dpeserta_didik_kesehatan)
      .delete('/rekening/:id', dpeserta_didik_rekening)
      .delete('/:id', dpeserta_didik)
      .patch('/alamat/:id', upeserta_didik_alamat)
      .patch('/kontak/:id', upeserta_didik_kontak)
      .patch('/kesehatan/:id', upeserta_didik_kesehatan)
      .patch('/rekening/:id', upeserta_didik_rekening)
      .patch('/:id', upeserta_didik)

module.exports = router